# EYSERVER VPS

Данный проект является агрегатором приложений и предназанчен
для развертования на удалённом сервере через docker-compose.
На сервере настроен nginx с автоматическим получением сертификатов для **https**.  
В данный момент сервер настроен для следующий сервисов:

- [bot_2816](https://gitlab.com/Daneel/bot2816)
- [telegram_timer](https://gitlab.com/Daneel/telegram_timer)

## Содержание
1. [Dependencies](#Dependencies)
2. [Настройка сервера](#Setup)
    + environment
    + bot_2816
    + telegram_timer
3. [Скрипт запуска](#Start)
4. [Обновление](#Update)

## <a name="Dependencies"></a>Зависимости

 * **docker-compose**

## <a name="Setup"></a>Настройка сервера

### Environment
Для настройки сервера необходимо задать пароли к его сервисам.
Настройка осуществляется через параметры окружения docker-compose
Создайте файл **.env**, в котором задайте основные параметры окружения:

    POSTGRES_PASSWORD=test
    RABBITMQ_ADMIN_PASSWORD=test

    RABBITMQ_BOT2816_PASSWORD=test
    RABBITMQ_TELEGRAM_TIMER_PASSWORD=test

    POSTGRES_TELEGRAM_TIMER_USER=telegram_timer
    POSTGRES_TELEGRAM_TIMER_PASSWORD=test
    POSTGRES_TELEGRAM_TIMER_DATABASE=telegram_timer

### bot_2816
Для конфигурации **bot_2816** создайте файл _data/bot2816/config.json_
со следующим содержимым:

    {
        "bot_token": "SOME_BOT_TOKEN",
        "broker": "amqp://bot2816:test@rabbitmq/bot2816",
        "web_hook": {
            "host": "example.org"
        }
    }

В поле **bot_token** необходимо задать токен бота telegramm.  
Поле **broker** необходимо сконфигурировать в соответсвии с параметрами
RABBITMQ в настройках окружения.
Правила кофигурации строки брокера смотрите в
[документации Celery](https://docs.celeryproject.org/en/latest/userguide/configuration.html#conf-broker-settings).    
**host** в случаи запуска для отладки можно оставить пустым.

### telegram_timer
Для конфигурации **telegram_timer** создайте файл
_data/telegram_timer/config.json_
со следующим содержимым:

    {
        "bot_token": "SOME_BOT_TOKEN",
        "broker": "amqp://telegram_timer:test@rabbitmq/telegram_timer",
        "webhook_host": "example.org",
        "database_connection": "postgresql://telegram_timer:test@postgres/telegram_timer",
        "input_timezone": "Europe/Minsk"
    }

В поле **bot_token** необходимо задать токен бота telegramm.  
Поле **broker** необходимо сконфигурировать в соответсвии с параметрами
RABBITMQ в настройках окружения.
Правила кофигурации строки брокера смотрите в
[документации Celery](https://docs.celeryproject.org/en/latest/userguide/configuration.html#conf-broker-settings).  
**database_connection** необходимо сконфигурировать в соответсвии с параметрами POSTGRESS.
Правила кофигурации строки подключения к БД смотрите в
[документации sqlalchemy](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.create_engine).  
**host** в случаи запуска для отладки можно оставить пустым.

## <a name="Start"></a>Скрипт запуска

Сервер пооддерживает несколько профилей запуска. Для упрощения запуска
был сделано пару небольших скриптов. Скрипты имеет следующий синтаксис:

    ./start prod|debug [ARGS]

    ./stop prod|debug [ARGS]

и поддерживает следующие конфигурации:

 + debug - развертывание без веб сервера.
 Имеет и другие упрощения для разработки. Не предназначенно для production.
 + prod - развертывание через nginx.

Оптицональные аргументы передаются **docker-compose**.

## <a name="Update"></a>Обновление

Для рекурсивного обновления через git можно использолвать слудующую команду:

    ./update
