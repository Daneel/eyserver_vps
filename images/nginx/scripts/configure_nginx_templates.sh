VARS_TO_REPLACE='$BOT2816_DOMAIN:$EYSERVER_DOMAIN:$TELEGRAM_TIMER_DOMAIN'

for ifile in /etc/nginx/conf.template.d/*.conf.template;
do
    basename=$(basename $ifile .conf.template);
    ofile=/etc/nginx/conf.d/$basename.conf
    envsubst "$VARS_TO_REPLACE" <$ifile >$ofile
done
