#!/bin/bash

envsubst '$BOT2816_PASSWORD:$TELEGRAM_TIMER_PASSWORD:$ADMIN_PASSWORD' \
    </etc/rabbitmq/definitions.json.template \
    >/etc/rabbitmq/definitions.json
