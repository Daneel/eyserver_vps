#!/usr/bin/env bash


SCRIPT_DIR=`dirname $0`


start_prod()
{
    docker-compose \
        -f $SCRIPT_DIR/docker-compose.yml \
        -f $SCRIPT_DIR/docker-compose.prod.yml \
        up --build \
        $@
}


start_debug()
{
    docker-compose \
        -f $SCRIPT_DIR/docker-compose.yml \
        -f $SCRIPT_DIR/docker-compose.debug.yml \
        up --build \
        $@
}


run_help()
{
    echo "Usages: $0 prod|debug [ARGS]"
}


case $1 in
    prod)
        shift
        start_prod $@
        ;;
    debug)
        shift
        start_debug $@
        ;;
    *)
        run_help
        ;;
esac
