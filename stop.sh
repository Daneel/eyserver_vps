#!/usr/bin/env bash


SCRIPT_DIR=`dirname $0`


stop_prod()
{
    docker-compose \
        -f $SCRIPT_DIR/docker-compose.yml \
        -f $SCRIPT_DIR/docker-compose.prod.yml \
        down \
        $@
}


stop_debug()
{
    docker-compose \
        -f $SCRIPT_DIR/docker-compose.yml \
        -f $SCRIPT_DIR/docker-compose.debug.yml \
        down \
        $@
}


run_help()
{
    echo "Usages: $0 prod|debug [ARGS]"
}


case $1 in
    prod)
        stop_prod
        ;;
    debug)
        stop_debug
        ;;
    *)
        run_help
        ;;
esac
